//      Создать простую HTML страницу с кнопкой Вычислить по IP.
//     По нажатию на кнопку - отправить AJAX запрос по адресу https://api.ipify.org/?format=json, получить оттуда IP адрес клиента.
//     Узнав IP адрес, отправить запрос на сервис https://ip-api.com/ и получить информацию о физическом адресе.
//     Под кнопкой вывести на страницу информацию, полученную из последнего запроса - континент, страна, регион, город, район города.


const button = document.querySelector('button');
const info = document.querySelector('.info_area');
const linkForIp = 'https://api.ipify.org/?format=json';
const linkForIpInfo = 'http://ip-api.com/';

button.addEventListener("click", () => {
    info.innerHTML = '';
    fetch(linkForIp).then(response => {
        if(response.status !==200){
           printOnPage('Status Code: ' + response.status, info)
           return;
        }
        return response.json();
    }).then(data => 
    {
        printOnPage(data.ip, info);
        fetch(linkForIpInfo + 'json/' + data.ip).then(response => {
            return response.json();
        }).then(data => {
            const string = `<p>Континент: ${data['timezone'].split('/')[0]}</p><p>Страна: ${data['country']}</p><p>Регион: ${data['regionName']}</p><p>Город: ${data['city']}</p><p>Код района: ${data['zip']}</p>`;
            printOnPage(string, info)
        }).catch(error =>{
            console.log(error)
            printOnPage('<p>'+error+'</p>', info)
        })
    })
        .catch(error =>{
        printOnPage('<p>'+error+'</p>', info)
    })
})

function printOnPage(data, node){
    node.innerHTML += data
}