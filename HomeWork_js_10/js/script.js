window.onload = () => {
    // Отбираю элементы
    const error = document.querySelector('.error');
    const submit = document.querySelector('.form__button');
    const look = document.querySelectorAll('.form__group i');
    const inputs = document.querySelectorAll('.form__input');

    look.forEach(item => {
        item.addEventListener('click', () => { // Вешаю клик на иконки, на родителя тут особо смысла не видю )
            toggleClasses(item, 'fa-eye fa-eye-slash'); // Переключаю вид иконок
            const inputField = item.closest('.form__group').firstElementChild; // Отбираю поле ввода нажатой иконки
            fieldSwitch(inputField); // Меняю тип поля ввода
        })
    })

    submit.addEventListener('click', (event) => {
        event.preventDefault();
        if (inputs[0].value === inputs[1].value) { // Сравниваю значения полей при нажатии подтверждения
            alert('Welcome!'); // Все ок
            inputs[0].value = inputs[1].value = ""; // Обнуляю поля.
        } else {
            error.classList.add('error-active'); // При несовпадении вывожу ошибку
        }
    })

    inputs.forEach(item => {
        item.addEventListener('input', () => error.classList.remove('error-active')); // По изменению в полях ввода убираю ошибку
    })

    function fieldSwitch(field) { // Смена типа поля
        if (field.type === 'password') {
            field.type = 'text'
        } else {
            field.type = 'password';
        }
    }

    function toggleClasses(elem, classes) { // Чтобы меньше писать если работа с несколькими класами
        for (const classElem of classes.split(' ')) {
            elem.classList.toggle(classElem);
        }
        return this;
    }
}