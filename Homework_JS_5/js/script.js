// Возьмите выполненное домашнее задание номер 4 (созданная вами функция createNewUser()) и дополните ее следующим функционалом:
//
//     При вызове функция должна спросить у вызывающего дату рождения (текст в формате dd.mm.yyyy) и сохранить ее в поле birthday.
//     Создать метод getAge() который будет возвращать сколько пользователю лет.
//     Создать метод getPassword(), который будет возвращать первую букву имени пользователя в верхнем регистре, соединенную с фамилией (в нижнем регистре) и годом рождения. (например, Ivan Kravchenko 13.03.1992 → Ikravchenko1992).
//
//
// Вывести в консоль результат работы функции createNewUser(), а также функций getAge() и getPassword() созданного объекта.


let uzver = createNewUser();

let login = uzver.getLogin();
let password = uzver.getPassword();
let age = uzver.getAge();

for (let key in uzver) {
    console.log("Key: " + key + " value: " + uzver[key]);
}
console.log("First: " + uzver.firstName + "; Last: " + uzver.lastName);
console.log(`Your login is "${login}"`);
console.log(`Your password is "${password}"`);
console.log(`Your age is "${age}"`);


function createNewUser() {
    const user = {
        firstName: prompt("Enter first name") || "NoFirstName",
        lastName: prompt("Enter last name") || "NoLastName",
        birthday: getBirhday(),

    };
    Object.defineProperties(user, {
            "firstName": {
                writable: false,
                configurable: true,
                enumerable: true
            },
            "lastName": {
                writable: false,
                configurable: true,
                enumerable: true
            },
            "birthday": {
                writable: false,
                configurable: true,
                enumerable: true
            },
            "setFirstName": {
                set(value) {
                    Object.defineProperty(this, "firstName", {value})
                },
                enumerable: false,
            },
            "setLastName": {
                set(value) {
                    Object.defineProperty(this, "lastName", {value})
                },
                enumerable: false,
            },
            "getLogin": {
                value: function () {
                    return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
                },
                enumerable: true,
            },
            "getAge": {
                value: function () {
                    let currDate = new Date();
                    currDate.setHours(0);
                    currDate.setMinutes(0);
                    currDate.setSeconds(0);
                    return Math.floor((currDate - this.birthday) / 31556952000);
                },
                enumerable: true,
            },
            "getPassword": {
                value: function () {
                    return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.getFullYear().toString();
                },
                enumerable: true,
            },
        }
    );
    return user;

    function getBirhday() {
        let sDate = prompt("Введите дату рождения в виде \"dd.mm.yyyy\"") || "no.date";
        let birthdayArr = sDate.split(".");
        let date = new Date(`${birthdayArr[2]},${birthdayArr[1]},${birthdayArr[0]}`);
        if (isNaN(date.getDate()) || isNaN(date.getMonth()) || isNaN(date.getFullYear())) {
            alert('Пожалуйста, введите правильную дату рождения в виде \'dd.mm.yyyy\':');
            return getBirhday();
        }
        return date;
    }
}

// -------------------

