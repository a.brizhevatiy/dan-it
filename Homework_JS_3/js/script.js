let firstDigit = digitCheck(prompt("First number:"));
let secondDigit = digitCheck(prompt("Second number:"));
let operation = operationCheck(prompt("Enter + or - or * or /"));

console.log(numberOperations(firstDigit, secondDigit, operation));

function numberOperations(num1, num2, operation) {
    switch (operation) {
        case "+":
            return num1 + num2;
        case "-":
            return num1 - num2;
        case "/":
            return num1 / num2;
        case "*":
            return num1 * num2;
    }
}

function digitCheck(num) {
    if (!isNaN(+num)) {
        return +num;
    } else {
        for (; ;) {     //Идентичен while()
            if (isNaN(+num)) {
                num = prompt("You need to enter the digit:");
            } else {
                return +num;
            }
        }
    }
}

function operationCheck(op) {
    let operations = ["+", "-", "*", "/"];
    if (operations.includes(op)) {
        return op;
    } else {
        for (; ;) {      //Идентичен while()
            op = prompt("Wrong. Enter + or - or * or / ");
            if (!operations.includes(op)) {
                continue;
            } else {
                return op;
            }
        }
    }
}