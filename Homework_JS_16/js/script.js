// Написать функцию для подсчета n-го обобщенного числа Фибоначчи. Аргументами на вход будут три числа - F0, F1, n, где F0, F1 -
// первые два числа последовательности (могут быть любыми целыми числами), n - порядковый номер числа Фибоначчи, которое надо найти. Последовательнось будет строиться по следующему правилу F2 = F0 + F1, F3 = F1 + F2 и так далее.
//     Считать с помощью модального окна браузера число, которое введет пользователь (n).
//     С помощью функции посчитать n-е число в обобщенной последовательности Фибоначчи и вывести его на экран.
//     Пользователь может ввести отрицательное число - результат надо посчитать по такому же правилу (F-1 = F-3 + F-2).


window.onload = function () {
    let num = prompt('Введите число последовательности');

    while (!digitCheck(num)) {
        num = prompt(`Вы ввели "${num}", пожалуйста, введите корректное число:`);
    }
    num = parseInt(num);
    if (num > 0) {
        alert(fibonacci(num));
    } else {
        alert(fiboNeg(num));
    }

};

function fibonacci(num, f0 = 1, f1 = 1) {
    if (num === 1) {
        return f0;
    } else if (num === 2) {
        return f1
    } else {
        return fibonacci(num - 1, f1, f0 + f1);
    }
}

function fiboNeg(num, f0 = -1, f1 = 1) {
    if (num === 0) {
        return 0
    } else if (num === -1) {
        return f1;
    } else if (num === -2) {
        return f0;
    } else {
        return fiboNeg(num + 1, f1 - f0, f0);
    }
}

function digitCheck(num) {
    return num !== null && !isNaN(parseInt(num));
}

