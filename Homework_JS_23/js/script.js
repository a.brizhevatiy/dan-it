window.addEventListener('DOMContentLoaded', () => {
    const startButton = document.querySelector('.start-btn');
    const restartButton = document.createElement('button');
    let playing = false;
    restartButton.classList.add('start-btn');
    startButton.innerHTML = 'Go-go-go!';
    restartButton.innerHTML = 'Restart';
    document.body.appendChild(startButton);
    const fields = document.createElement('div');
    fields.classList.add('field-grid');
    const mineLogo = '>*<';
    const askLogo = '?';
    let gameField = [];
    let size;
    let mines;
    let flags;


    restartButton.addEventListener('click', () => {
        blow();
        start();
    })

    startButton.addEventListener("click", () => {
        if (playing) return;
        start();
    })


    fields.addEventListener('click', function (event) { // открываем клетки
        document.body.appendChild(restartButton);
        const target = event.target;
        let rule = target.classList.contains('fields') && target.innerHTML !== askLogo;
        if (rule) {
            if (target.dataset.bomb === '1') {
                blow();
                return false;
            } else {
                target.innerHTML = itemCount(target, isMine);
                target.style.backgroundColor = 'white';
            }
        }
        if(target.innerHTML === ' '){
            opens(target);
        }
    })

    fields.addEventListener('contextmenu', function (event) { // вешаем флажки
        const target = event.target;
        event.preventDefault();
        const rule = target.classList.contains('fields') && !target.style.backgroundColor;
        if (rule) {
            if (target.innerHTML === '') {
                if (flags <= 0) {
                    return
                }
                target.innerHTML = askLogo;
                flags -= 1;
                target.dataset.flag = '1';
            } else {
                target.innerHTML = '';
                flags += 1;
                target.dataset.flag = '0';
            }
        }
        startButton.innerHTML = `Flags: ${flags} / Mines: ${mines}`;
    })

    fields.addEventListener('dblclick', function (event) { //Открываем соседей по двойному клику
        const target = event.target;
        const rule = target.classList.contains('fields') && target.style.backgroundColor === 'white';
        if (rule) {
            opens(event.target)
        }
    })

    function start() { // старт игры и отрисовка поля
        size = parseInt(prompt('Введите размер поля (большее или равно 8)'));
        while (isNaN(size) || size < 8) {
            size = parseInt(prompt('Введите размер поля (большее или равно 8)'));
        }
        mines = Math.floor(size * size / 6)
        flags = mines;
        fields.style.gridTemplateRows = `repeat(${size})`;
        fields.style.gridTemplateColumns = `repeat(${size}, 52px)`;
        startButton.style.removeProperty('background-color');
        startButton.style.removeProperty('font-size');
        while (fields.firstChild) {
            fields.removeChild(fields.firstChild);
        }
        fields.style.pointerEvents = 'inherit';
        playing = true;
        fieldCreate(size);
        startButton.innerHTML = `Flags: ${flags} / Mines: ${mines}`;
    }

    function fieldCreate(size) { //создаем матрицу поля для удобства навигации, в датасеты - координаты
        for (let i = 0; i < size; i++) {
            const arr2 = [];
            for (let j = 0; j < size; j++) {
                const field = document.createElement("div");
                field.classList.add('fields');
                field.dataset.x = i + '';
                field.dataset.y = j + '';
                arr2.push(field);
                fields.appendChild(field);
            }
            gameField.push(arr2);
        }
        document.body.appendChild(fields);
        bombing(mines);
    }

    function itemCount(item, myFunction) { //обход соседей для подсчета в данном случае или бомб или флагов
        let i = 0;
        const itemX = item.dataset.x;
        const itemY = item.dataset.y;
        for (let x = parseInt(itemX) - 1; x <= parseInt(itemX) + 1; x++) {
            if (x < 0 || x >= size) {
                continue;
            }
            for (let y = parseInt(itemY) - 1; y <= parseInt(itemY) + 1; y++) {
                if (y < 0 || y >= size || (x + '' === itemX && y + '' === itemY)) {
                    continue;
                }
                i += myFunction(gameField[x][y]);
            }
        }
        return i === 0 ? ' ' : i;
    }

    function bombing(mines) { // разброс бомб по полю
        for (let i = 0; i < mines;) {
            let x = randNum(0, (size - 1))
            let y = randNum(0, (size - 1))
            if (gameField[x][y].dataset.bomb === '1') {
                continue;
            }
            gameField[x][y].dataset.bomb = '1';
            i++;
        }
    }

    function randNum(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    function isMine(item) {
        if (item.dataset.bomb === '1') return 1;
        else return 0;
    }

    function isFlag(item) {
        if (item.dataset.flag === '1') return 1;
        else return 0;
    }

    function blow() { // Упс, бомба.....
        document.querySelectorAll('[data-bomb="1"]').forEach(item => {
            item.style.backgroundColor = 'red';
            item.innerHTML = mineLogo;
            fields.style.pointerEvents = 'none';
            playing = false;
            startButton.style.backgroundColor = 'red';
            startButton.style.fontSize = '12px';
            startButton.innerHTML = `Press to play again.`
            restartButton.remove();
            gameField = [];
        })
        return false;
    }

    function opens(item) { //обход соседей для открытия
        if (item.innerHTML !== ' ' && itemCount(item, isMine) !== itemCount(item, isFlag)) {
            return;
        }
        const itemX = item.dataset.x;
        const itemY = item.dataset.y;
        for (let x = parseInt(itemX) - 1; x <= parseInt(itemX) + 1; x++) {
            if (x < 0 || x >= size) {
                continue;
            }
            for (let y = parseInt(itemY) - 1; y <= parseInt(itemY) + 1; y++) {
                if (y < 0 || y >= size || (x + '' === itemX && y + '' === itemY)) {
                    continue;
                }
                const field = gameField[x][y];
                if (isMine(field) && !isFlag(field)) { // если флагов столько же сколько и бомб, но поле с бомбой не отмечено - бум!
                    blow();
                    return false;
                }
                if (!isMine(field) && field.innerHTML === '') {
                    field.style.backgroundColor = 'white';
                    field.innerHTML = itemCount(field, isMine);
                    if (itemCount(field, isMine) === " ") {
                        field.innerHTML = itemCount(field, isMine);
                        opens(field);
                    }
                }
            }
        }
    }
})