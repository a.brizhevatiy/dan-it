window.onload = function () {
    let num = prompt('Введите число для факториала:');
        while (!digitCheck(num)) {
            num = prompt(`Вы ввели "${num}", пожалуйста, введите корректное и не отрицательное число:`);
        }
    alert(factorial(num));
};

function factorial(num) {
    if (num == 0 || num === 1) {
        return 1;
    } else {
        return num * factorial(num - 1);
    }
}

function digitCheck(num) {
    return num !== null && !isNaN(parseInt(num)) && num > -1;
}