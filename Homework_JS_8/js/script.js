window.onload = function (f) {
    // Выбираю поле для ввода:
    const input = document.querySelector('.priceValue');
    // Создаю DIV для отображения цены и крестика:
    const output = document.createElement('div');
    output.classList.add('output')
    // Создаю SPAN для отображения предупреждения:
    const warning = document.createElement('span');
    warning.innerHTML = 'Please enter correct price.';
    warning.classList.add('warning')
    // Создаю SPAN для отображения цены из поля ввода
    const currentPrice = document.createElement('span');
    currentPrice.classList.add('currentPrice')
    // Создаю SPAN для отображения закрывающего крестика:
    const close = document.createElement('span');
    close.classList.add('close')
    // Выбираю контент:
    const content = document.querySelector('.content');
    // Объединяю SPAN`s в DIV'е
    output.appendChild(currentPrice);
    output.appendChild(close);
    // Вешаю обработчик фокуса на поле ввода:
    input.addEventListener('focus', () => {
        input.classList.add('focus');
    });
    // Вешаю обработчик на поле ввода, когда убирается фокус:
    input.addEventListener('blur', () => {
        input.classList.remove('focus');
        // Забираю значение из поля ввода:
        let value = parseFloat(input.value);
        // Проверяю на пустое поле и 0, делаю оформление поля по умолчанию в таком случае:
        if (input.value === '' || value === 0){
            output.remove();
            warning.remove();
            input.classList.remove('error');
        }
        // Проверяю на отрицательные данные в поле, оформляю ошибку:
        else if (value < 0) {
            input.style.removeProperty('color');
            input.classList.add('error');
            content.appendChild(warning);
            output.remove();
        } else {
            // При корректных данных:
            input.style.color = 'yellowgreen';
            input.classList.remove('error')
            content.insertBefore(output, document.querySelector('.price'));
            warning.remove();
            currentPrice.innerHTML = `Текущая цена: ${value}<small>$</small>`;
            // Вешаю обработчик на нажатие на крестик:
            close.addEventListener('click', () => {
                output.remove();
                input.value = '';
            });
        }
    });
}

