let obj = { //Подопытный кролик.
    1: 1,
    2: 2,
    3: 3,
    4: {
        11: 11,
        12: 12,
        13: {
            "one": 21,
            "two": 22,
            "Three": {
                "po": "qwerty",
                "pu": "qwerty",
                "pi": "qwerty",
                "doo": "qwerty",
            },
            "four": 24,
        },
        14: 14,
        15: 15,
        16: 16,
    },
    5: 5,
    6: 6,
    7: 7,
    8: 8,
};

let newObj = copyAll(obj);

function copyAll(obj) {
    let copy;
// Обработка трёх простых типов и null или undefined
    if (null == obj || "object" != typeof obj) return obj;
// Обработка дат
    if (obj instanceof Date) {
        copy = new Date();
        copy.setTime(obj.getTime());
        return copy;
    }
// Обработка массивов
    if (obj instanceof Array) {
        copy = [];
        for (let i = 0, len = obj.length; i < len; i++) {
            copy[i] = copyAll(obj[i]);
        }
        return copy;
    }
// Обработка функций
    if (obj instanceof Function) {
        copy = function() {
            return obj.apply(this, arguments);
        };
        return copy;
    }
// Обработка объектов
    if (obj instanceof Object) {
        copy = {};
        for (let attr in obj) {
            if (obj.hasOwnProperty(attr)) copy[attr] = copyAll(obj[attr]);
        }
        return copy;
    }
}
console.log(obj);
console.log(newObj);

console.log('\nobj[4][13]["Three"]["pu"] = "KaVedDFioO";')
obj[4][13]["Three"]["pu"] = "KaVedDFioO";
console.log(obj[4][13]["Three"]["pu"]);
console.log(newObj[4][13]["Three"]["pu"]);
console.log('\ndelete obj[4][13]["Three"]:');
delete obj[4][13]["Three"];
console.log(obj[4][13]["Three"]);
console.log(newObj[4][13]["Three"]);
console.log('\nnewObj[4][13]["Three"] = "asdzxcvadszxfvaf":');
newObj[4][13]["Three"] = "asdzxcvadszxfvaf";
console.log(obj[4][13]["Three"]);
console.log(newObj[4][13]["Three"]);