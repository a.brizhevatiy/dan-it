window.onload = () => {

    const images = document.querySelectorAll('.gallery-1 img');
    const btnNext = document.querySelector('.gallery-1 .buttons .next');
    const btnPrev = document.querySelector('.gallery-1 .buttons .prev');
    let i = 0;
    let flag;

    btnNext.onclick = next;
    btnPrev.onclick = prev;

    function next() {
        slide(i);
    }

    function prev() {
        slide(i, 'reverse');
    }

    function slide(count, direction) {
        if (flag) {
            return
        }
        flag = true;
        let curPosFrom, curPosTo, nxtPosFrom, nxtPosTo;
        const curImg = images[i];
        if (direction === 'reverse') {
            curPosFrom = 0;
            curPosTo = 110;
            nxtPosFrom = -110;
            nxtPosTo = 0;
            i++;
            if (i >= images.length) {
                i = 0;
            }
        } else {
            curPosFrom = 0;
            curPosTo = -110;
            nxtPosFrom = 110;
            nxtPosTo = 0;
            i--;
            if (i < 0) {
                i = images.length - 1;
            }
        }
        const nextImg = images[i];
        nextImg.style.left = `${nxtPosFrom}%`;
        nextImg.classList.add('showed');
        const timer = setInterval(() => {
            if (curPosFrom > curPosTo) {
                curPosFrom -= 1;
                nxtPosFrom -= 1;
                curImg.style.left = `${curPosFrom}%`;
                nextImg.style.left = `${nxtPosFrom}%`;
            } else if (curPosFrom < curPosTo) {
                curPosFrom += 1;
                nxtPosFrom += 1;
                curImg.style.left = `${curPosFrom}%`;
                nextImg.style.left = `${nxtPosFrom}%`;
            } else {
                curImg.classList.remove('showed');
                flag = false;
                clearInterval(timer);
            }
        }, 5)
    }
}

