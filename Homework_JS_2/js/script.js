let userNumber = prompt("Enter the number:")


while (isNaN(+userNumber)) {
    userNumber = prompt("The number, please:")
}

while (userNumber % 1 !== 0) {
    userNumber = prompt("Need Integer, not Float")
}


if (userNumber < 5) {
    console.log("Sorry, no numbers");
} else {
    for (let x = 5; x <= userNumber; x += 5) {
        console.log(x);
    }
}

alert("Here come advanced part!");

let firstNumber = prompt("Enter the first number:");
let secondNumber = prompt("Enter the second number:");

while (firstNumber >= secondNumber) {
    firstNumber = prompt("Lets try again. First number:");
    secondNumber = prompt("Second number should be bigger than first:");
}

lookingForPrime:
    for (; firstNumber <= secondNumber; firstNumber++) {
        if (firstNumber <= 1) {
            continue;
        }

        if (firstNumber === 2) {
            console.log(firstNumber);
            continue;
        }

        for (let x = 2; x < firstNumber; x++) {
            if (firstNumber % x === 0) {
                continue lookingForPrime;
            }
        }
        console.log(firstNumber);
    }
