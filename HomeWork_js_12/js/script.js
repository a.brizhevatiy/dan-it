window.onload = () => {
    const images = document.querySelectorAll('.image-to-show');
    const stopBtn = document.querySelector('.stop_btn');
    const startBtn = document.querySelector('.start_btn');
    let time = 10000;
    let step = 10;
    let count = 0;
    let flag = true;
    let timeToCount = time;

    let timeCounter = setTimeout(next, step);

    startBtn.addEventListener('click', () => {
        if (flag) return;
        flag = true;
        setTimeout(next, step);
    });

    stopBtn.addEventListener('click', () => {
        if (!flag) return;
        flag = false;
        clearTimeout(timeCounter);
    })

    function next() {
        if (timeToCount > 0) {
            document.querySelector(".display").innerHTML = (timeToCount/1000).toPrecision((timeToCount/10).toString().length);
            timeToCount -= 10;
            timeCounter = setTimeout(next, step)
        } else {
            document.querySelector(".display").innerHTML = (timeToCount/1000).toPrecision((timeToCount/10).toString().length);
            fade(images[count]);
            count++;
            if (count >= images.length) {
                count = 0;
            }
            fade(images[count], 'in');
            timeToCount = time;
            timeCounter = setTimeout(next, step)
        }
    }

    function fade(elem, inout = 'out') {
        if (elem === undefined) {
            return false;
        }
        const fps = 50; // кадров в секунду
        const time = 500; // время работы анимации
        let steps = time / (1000 / fps); // сколько всего покажем кадров
        const d0 = 1 / steps; // изменение прозрачности за 1 кадр
        if (inout === 'in') {
            let op = 0
            let timer = setInterval(() => {
                op += d0; // текущее значение opacity
                elem.style.opacity = op;
                if (op >= 1) {
                    clearInterval(timer);
                }
            }, (1000 / fps));
        } else if (inout === 'out') {
            let op = 1
            let timer = setInterval(() => {
                op -= d0; // текущее значение opacity
                elem.style.opacity = op;
                if (op <= 0) {
                    clearInterval(timer);
                }
            }, (1000 / fps));
        } else {
            console.error('Please, use "In" or "Out"(default) in second parameters or leave empty for this Fade');
            return false;
        }
    }

}
