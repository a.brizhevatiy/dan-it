window.onload = function () {
    let student = {
        name: prompt("Ввeдите имя"),
        lastName: prompt("Введите фамилию"),
        tabel: {},
    };

    let subject;
    let mark;
    while (true) {
        subject = prompt("Введите предмет:");
        if (subject in student.tabel){
            alert("Данный предмет уже был.")
            continue;
        }
        if (subject === null) {
            break;
        }
        mark = digitCheck(prompt(`Введите оценку по ${subject}:`));
        student.tabel[subject] = mark;
    }

    let badMarks = false;
    let count = 0;
    for (let sub in student.tabel) {
        if (student.tabel[sub] < 4) {
            badMarks = true;
        }
        count += parseInt(student.tabel[sub]);
    }


    if (!badMarks) {
        alert("Студент переведен на следующий курс.");
    }

    let averageMark = (count / Object.keys(student.tabel).length).toFixed(1);
    if (averageMark > 7 && !badMarks) {
        alert(`Средняя оценка: ${averageMark}.\nСтуденту назначена стипендия.`);
    } else {
        alert(`Средняя оценка: ${averageMark}.`);
    }
};

function digitCheck(num) {
    while (true) {
        if (num !== null && !isNaN(parseInt(num))) {
            return num;
        } else {
            num = prompt("You need to enter the digit:");
        }
    }
}