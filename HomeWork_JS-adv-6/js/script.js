class GameField {
    constructor(size) {
        this.gameField = new Table(size);
    }
}

class Table {
    constructor() {
        this._elem = document.createElement('table');
    }

    createTable(fieldSize) {
        for (let i = 0; i < fieldSize; i++) {
            const tr = document.createElement('tr');
            for (let j = 0; j < fieldSize; j++) {
                const cell = new Cell({
                    size: 40,
                })
                tr.append(cell.elem);
            }
            this._elem.append(tr);
        }
        this._elem.classList.add('table')
        const head = document.querySelector('.head');
        head.insertAdjacentElement('afterend', this._elem);
    }

    get table() {
        return this._elem;
    }

    addClass(className) {
        this._elem.classList.add(className);
    }
}

class Cell {
    constructor(options) {
        this.GAME_COLORS = { // Цвета для игры
            'DEFAULT_COLOR': '#C0C0C0',
            'ACTIVE_COLOR': '#0000FF',
            'WIN_COLOR': '#00FF00',
            'LOSE_COLOR': '#FF0000',
        }
        this._elem = document.createElement('td');
        this._elem.size = options.size;
        this._elem.style.width = this._elem.style.height = `${this._elem.size}px`
        this._elem.style.backgroundColor = this.GAME_COLORS.DEFAULT_COLOR;
        this._status = 0;
        this._inGame = false;
    }

    addClass(className) {
        this._elem.classList.add(className);
    }

    get elem() {
        return this._elem;
    }

    active() {
        this._elem.style.backgroundColor = this.GAME_COLORS.ACTIVE_COLOR;
        this._inGame = true;
    }

    good() {
        this._elem.style.backgroundColor = this.GAME_COLORS.WIN_COLOR;
        this._status = 1;
        this._inGame = false;
    }

    bad() {
        this._elem.style.backgroundColor = this.GAME_COLORS.LOSE_COLOR;
        this._status = -1;
        this._inGame = false;
    }
}

class Element {
    constructor(elem) {
        this._elem = document.querySelector('elem');
    }

    setContent(content) {
        this._elem.innerHTML = content;
    }
}

class Popup extends Element {
    constructor(elem) {
        super(elem);
    }

    show() {
        this._elem.style.display = 'block';
    }

    hide() {
        this._elem.style.display = 'none';
    }

}

// ----------------------------------------

const myTable = new Table();
myTable.createTable(10)
console.log(myTable.table)
    // clicker(myTable.table);
myTable.table.addEventListener('click', (event) => {
    console.log(event)
})

// ----------------------------------------
// class Element {
//     constructor(element) {
//         this.el = document.querySelector(element);
//     }
//
//     show() {
//         this.el.style.display = 'block';
//     }
//
//     hide() {
//         this.el.style.display = 'none';
//     }
//
//     setContent(content) {
//         this.el.innerHTML = content;
//     }
//
// }

// const _GAME_COLORS = { // Цвета для игры
//     'DEFAULT_COLOR': '#C0C0C0',
//     'ACTIVE_COLOR': '#0000FF',
//     'WIN_COLOR': '#00FF00',
//     'LOSE_COLOR': '#FF0000',
// }

// Объявляем общие переменные
const size = 10;
const startBtn = document.getElementById('start_btn');
const restartBtn = document.getElementById('restart_btn');
const startPopup = new Element('.popup__start');
const endPopup = new Element('.popup__endgame');
const userPointsField = new Element('.user-points');
const computerPointsField = new Element('.computer-points');
let timer = null;
let gameElement = null;
let inGame = false;
let userPoints = 0;
let computerPoints = 0;
let usedNumbers = [];
let gameStarted = false;

// //Вешаю клики на кнопки старта и рестарта
startBtn.addEventListener('click', () => {
    let timeout = parseInt(document.querySelector('input[name="diff"]:checked').value) // Устанавливаю таймаут
    startGame(timeout);
});

restartBtn.addEventListener('click', () => {
    reset();
    endPopup.hide();
    startPopup.show();
});
// ---

// Начало игры
function startGame(timeout) {
    startPopup.hide();
    gameStarted = true;
    fieldConstructor(size);
    const cells = document.querySelectorAll('td');
    cells.forEach(item => {
        cellConstructor({
            elem: item,
            size: 40,
        });
    });
    gaming(cells, timeout);
}

// Обработка нажатия на ячейку при игре
function clicker(elem) {
    elem.addEventListener('click', (event) => {
        const target = event.target;
        const check = target.nodeName === 'TD';
        // const check = target.nodeName === 'TD' && target === gameElement && inGame;
        if (check) { // Если вовремя нажал на правильную ячейку
            target.good();
            // win(gameElement);
            // lose(gameElement);
        }
    })
}

// Процесс игры
function gaming(cells, timeout) {
    if (gameStarted) { // Проверяем, запущена ли игра
        timer = setTimeout(() => { // Ставим таймаут для автооткрытия ячеек
            if (inGame) { // Если не успели нажать за таймаут
                lose(gameElement);
                gaming(cells, timeout);
            } else { // Открываем новую
                let cellNum = getCellNumber(0, size * size - 1)
                gameElement = cells[cellNum]
                open(gameElement);
                gaming(cells, timeout);
            }
        }, timeout);
    }
}

// Сброс переменных
function reset() {
    timer = null;
    gameElement = null;
    inGame = false;
    userPoints = 0;
    computerPoints = 0;
    usedNumbers = [];
    document.querySelector('.table').remove();
    userPointsField.setContent('0');
    computerPointsField.setContent('0');
}

// Проверка есть ли победитель
function checkToWin(points) {
    if (points === (size * size / 2)) {
        clearTimeout(timer);
        gameStarted = false;
        winner();
    }
}

// Объявление победителя
function winner() {
    const winner = userPoints > computerPoints ? 'Пользователь выиграл!' : 'Компьютер выиграл!'
    const winContent = `<p>${winner}</p>`;
    const win = new Element('.winText');
    win.setContent(winContent);
    endPopup.show();
}

// Возврат уникального номера для дальнейшего вызова ячейки
function getCellNumber(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    let cellNum = Math.floor(Math.random() * (max - min + 1)) + min;
    if (usedNumbers.indexOf(cellNum) !== -1) {
        return getCellNumber(min, max);
    } else {
        usedNumbers.push(cellNum);
        return cellNum;
    }
}

// Параметры открывающейся ячейки
function open(elem) {
    gameElement = elem;
    inGame = true;
    elem.style.backgroundColor = _GAME_COLORS.ACTIVE_COLOR;
}

// Вызывается если успели нажать в срок
function win(elem) {
    inGame = false;
    gameElement = null;
    elem.style.backgroundColor = _GAME_COLORS.WIN_COLOR;
    userPoints++;
    checkToWin(userPoints);
    userPointsField.setContent(userPoints);
}

// Вызывается если не успели нажать в срок
function lose(elem) {
    inGame = false;
    gameElement = null;
    elem.style.backgroundColor = _GAME_COLORS.LOSE_COLOR;
    computerPoints++;
    checkToWin(computerPoints);
    computerPointsField.setContent(computerPoints);
}

// Параметры для ячеек
function cellConstructor(options) {
    this.elem = options.elem;
    this.elem.size = options.size;
    this.elem.style.width = this.elem.style.height = `${this.elem.size}px`
    this.elem.style.backgroundColor = _GAME_COLORS.DEFAULT_COLOR;
}

// Функция запускающая создание Таблицы с Ячейками и добавления их
function fieldConstructor(size) {
    const table = createTable();
    createCells(size, table);
    const head = document.querySelector('.head');
    head.insertAdjacentElement('afterend', table);
    clicker(table);
}

// Функция создания элемента Таблицы
function createTable() {
    const emptyTable = document.createElement('table');
    emptyTable.classList.add('table');
    return emptyTable;
}

// Функция создания ячейки и добавления их в таблицу
function createCells(num, table) {
    for (let i = 0; i < num; i++) {
        const tr = document.createElement('tr');
        tr.innerHTML = '<td></td>'.repeat(num);
        table.appendChild(tr);
    }
}