// Написать функцию filterBy(), которая будет принимать в себя 2 аргумента. Первый аргумент - массив, который будет содержать
// в себе любые данные, второй аргумент - тип данных.
//     Функция должна вернуть новый массив, который будет содержать в себе все данные, которые были переданы в аргумент,
//     за исключением тех, тип которых был передан вторым аргументом. То есть, если передать массив ['hello', 'world', 23, '23', null],
//     и вторым аргументом передать 'string', то функция вернет массив [23, null].

let x = ['hello', 'world', 23, '23', null, true, null, null];


function filterBy(arr, datatype) {
    let newArr = [];
   for (let item in arr) {
        if (typeof arr[item] !== datatype) {
            newArr.push(arr[item]);
        }
    };
    return newArr;
}


// function filterNew(arr, datatype) {
// return arr.filter(item => typeof item !== datatype)
// }


let res1 = filterBy(x, 'object');
// let res2 = filterNew(x, 'object');

console.log(res1);
// console.log(res2);
