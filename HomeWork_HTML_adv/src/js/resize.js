const shortText = '- Last Instagram Shot';
const longText = '- Latest Instagram Shots';
const shotTitle = $('.shots__last-short')
const photos = $('.shots-images__item')
const photoArea = $('.shots-images')
const photoSize = 285;
const instaText = $('.shots__insta');
const shots = $('.shots')
let showPhotos = Math.floor(photoArea.width() / photoSize);
let windowSize = 320;

show();

$(window).resize(function () {
    if (Math.floor(photoArea.width() / photoSize) !== showPhotos) {
        showPhotos = Math.floor(photoArea.width() / photoSize);
        show();
    }

    if (window.innerWidth >= 768) {
        if (windowSize === 768) return;
        windowSize = 768;
        menuList.fadeIn(1);
    } else {
        if (windowSize === 320) return;
        windowSize = 320;
        menuList.fadeOut(1)
        buttonData.html(contentTmp);
    }
})

function show() {
    if (showPhotos > 1) {
        shotTitle.text(longText);
        instaText.css('display', 'inline')
        shots.css('background-color', 'rgba(0, 0, 0, 0.03)')
    } else {
        shotTitle.text(shortText);
        instaText.css('display', 'none')
        shots.css('background-color', 'white')
    }
    photos.each(function (index, item) {
        if (index < showPhotos) {
            $(item).fadeIn(200);
        } else {
            $(item).fadeOut(200);
        }
    })
}

