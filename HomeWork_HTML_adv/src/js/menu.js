const menuButton = $('.title-menu-icon');
const menuList = $('.title-menu-nav');
const buttonData = $('.title-menu-icon__lines');
const contentTmp = buttonData.html();
const x = '&times;';

menuButton.on('click', function (event) {
    if (menuList.css('display') === 'none') {
        menuList.fadeIn(200);
        buttonData.html(x);
    } else {
        menuList.fadeOut(200);
        buttonData.html(contentTmp);
    }
})