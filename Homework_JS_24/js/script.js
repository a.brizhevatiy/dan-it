window.onload = () => {
    const input = document.querySelector('input');
    const keys = document.querySelector('.keys');
    const mLetter = document.querySelector('.mem');
    const signs = '/*-+';
    let operation = '';
    let number1 = '';
    let number2 = '';
    let mem = 0;
    let mStatus = false;
    input.value = '0';

    const display = function (text) {
        if (text) {
            number2 += text;
            input.value = number2;
        } else {
            return input.value;
        }
    }

    const equally = function () {
        let n1 = parseFloat(number1);
        let n2 = parseFloat(number2);
        switch (operation) {
            case '+':
                return n1 + n2;
            case '-':
                return n1 - n2;
            case '*':
                return n1 * n2;
            case '/':
                if (n2 !== 0 && !isNaN(n2)) {
                    return n1 / n2;
                } else if (isNaN(n2)) {
                    return n1 / n1;
                } else {
                    console.error('Divine by ZERO!');
                    return 'Err.';
                }
        }
    }

    const operations = function (val) {
        if (!operation) {
            number1 = display();
            number2 = '';
            operation = val;
        } else {
            if (number2 === '') {
                operation = val;
                return;
            }
            number1 = equally() + '';
            number2 = '';
            operation = val;
            input.value = number1;
        }
    }

    const reset = function () {
        number1 = '';
        number2 = '';
        operation = '';
    }

    window.addEventListener('keydown', (event) => {
        const button = event.key;

        if (button === 'Escape') {
            input.value = '0';
            reset();
        }
        if (!isNaN(+button) || button === '.') {
            if (button === '.' && input.value.includes('.')) {
                return;
            }
            display(button);
        } else if (signs.includes(button)) {
            operations(button);
        } else if (button === 'Enter') {
            if (number1 === '' || number2 === '') {
                return false;
            }
            input.value = equally();
            reset();
        }
    })

    keys.addEventListener('click', (event) => {
        const button = event.target;

        if (button.value !== "mrc") {
            mStatus = true;
        }

        if (button.value === 'C') {
            input.value = '0';
            reset();
        }
        if (!isNaN(+button.value) || button.value === '.') {
            if (button.value === '.' && input.value.includes('.')) {
                return;
            }
            display(button.value);
        } else if (signs.includes(button.value)) {
            operations(button.value);
        } else if (button.value === '=') {
            if (number1 === '' || number2 === '') {
                return false;
            }
            input.value = equally();
            reset();
        } else if (button.value === "m+") {
            mem += parseFloat(display());
            number2 = '';
            mLetter.style.opacity = 1;
        } else if (button.value === "m-") {
            mem -= parseFloat(display());
            number2 = '';
            mLetter.style.opacity = 1;
        } else if (button.value === "mrc" && mStatus) {
            mStatus = false;
            number2 = mem + '';
            input.value = number2;
        } else if (button.value === "mrc" && !mStatus) {
            mem = 0;
            mLetter.style.opacity = 0;
        }
    })
}