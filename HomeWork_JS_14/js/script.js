$(document).ready(function () {
    const upButton = $('.button-up');
    const wrapper = $('.wrapper');
    const showButton = $('.show-button');

    $('.link').on('click', function (event) {
        event.preventDefault();
        const selector = $(this).attr('href');
        const h = $(selector);
        wrapper.animate({
            scrollTop: h.offset().top
        }, 'slow');
    })
    upButton.on('click', function () {
        wrapper.animate({
            scrollTop: 0
        }, 'slow');
    })
    showButton.on('click', function () {
        $('.g-con-gallery').slideToggle(1000);
        (showButton.html() === 'show news') ? showButton.html('hide news') : showButton.html('show news');
    })

    const scrollCheck = function(){
        const brLine = window.innerHeight;
        if ($(this).scrollTop() > brLine) {
            upButton.fadeIn('slow');
        } else {
            upButton.fadeOut('slow');
        }
    }
    scrollCheck();
    wrapper.on('scroll', scrollCheck);
})