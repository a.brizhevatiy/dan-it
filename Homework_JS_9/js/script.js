window.onload = () => {
    const tabs = document.querySelector('.tabs'); // Отбираю родителя вкладок
    const menuTabs = document.querySelectorAll('.tabs-title'); // Отбираю вкладки
    tabs.addEventListener('click', function(event) { // На родителе вешаю клик
        menuTabs.forEach(function(item) {
            item.classList.remove('active'); // Убираю Активный статус для всех вкладок
            document.getElementById(item.getAttribute('data-item')).classList.remove('active-content'); // Убираю
            // Активный статус для текста по вкладкам. Data-item как id текста.
        })
        if (event.target.classList.contains('tabs-title')) { // Проверяю что нажата вкладка
            event.target.classList.add('active'); // Добавляю ей активный класс
            document.getElementById(event.target.getAttribute('data-item')).classList.add('active-content'); // Добавляю активный класс связанному тексту
        }
    });
}git sdtatus