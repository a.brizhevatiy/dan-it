//      Создать простую HTML страницу с кнопкой Вычислить по IP.
//     По нажатию на кнопку - отправить AJAX запрос по адресу https://api.ipify.org/?format=json, получить оттуда IP адрес клиента.
//     Узнав IP адрес, отправить запрос на сервис https://ip-api.com/ и получить информацию о физическом адресе.
//     Под кнопкой вывести на страницу информацию, полученную из последнего запроса - континент, страна, регион, город, район города.
const button = document.querySelector('button');
const info = document.querySelector('.info_area');
const linkForIp = 'https://api.ipify.org/?format=json';
const linkForIpInfo = 'http://ip-api.com/';

button.addEventListener("click", () => {
    IllGetYouByIP()
})

function printOnPage(data, node) {
    node.innerHTML += data
}

async function IllGetYouByIP() {
    info.innerHTML = '';
    try {
        let responseIP = await fetch(linkForIp);
        let ip = await responseIP.json();
        printOnPage(ip.ip, info);
        if (responseIP.status !== 200) {
            printOnPage('Status Code: ' + responseIP.status, info)
            return
        }
        let responseLoc = await fetch(linkForIpInfo + 'json/' + ip.ip)
        if (responseLoc.status !== 200) {
            printOnPage('Status Code: ' + responseLoc.status, info)
            return
        }
        let position = await responseLoc.json();
        const string = `<p>Континент: ${position['timezone'].split('/')[0]}</p><p>Страна: ${position['country']}</p><p>Регион: ${position['regionName']}</p><p>Город: ${position['city']}</p><p>Код района: ${position['zip']}</p>`;
        printOnPage(string, info)
    } catch (err) {
        printOnPage('Something is wrong (((', info)
        console.error(err)
    }
}