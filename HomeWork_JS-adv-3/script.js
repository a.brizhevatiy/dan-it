// Реализовать класс Employee, в котором будут следующие свойства - name (имя), age (возраст), salary (зарплата). Сделайте так, чтобы эти свойства заполнялись при создании объекта.
//     Сделайте геттеры и сеттеры для этих свойств.
//     Сделайте класс Programmer, который будет наследоваться от класса Employee, у которого будет свойство lang (список языков)
// Для класса Programmer перезапишите геттер для свойства salary. Пусть он возвращает свойство salary умноженное на 3.
// Создайте несколько экземпляров обьекта Programmer, выведите их в консоль.
class Employer {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name;
    }

    set name(value) {
        this._name = value;
    }

    get age() {
        return this._age;
    }

    set age(value) {
        this._age = value;
    }

    get salary() {
        return this._salary;
    }

    set salary(value) {
        this._salary = value;
    }
}

class Programmer extends Employer {
    constructor(lang, ...restParams) {
        super(...restParams);
        this.lang = lang;
    }

    get salary() {
        return this._salary * 3;
    }

    set salary(value) {
        super.salary = value;
    }
}

const employer = new Employer('test1', 25, 1000);
const prog1 = new Programmer(['java', 'Python'], 'test2', 25, 1000);
const prog2 = new Programmer(['js', 'jQuery'], 'test3', 35, 2000);
const prog3 = new Programmer(['C#', 'C++'], 'test4', 45, 3000);


console.log(employer, prog1, prog2, prog3)