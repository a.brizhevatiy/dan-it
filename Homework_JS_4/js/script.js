// Технические требования:
//
//     Написать функцию createNewUser(), которая будет создавать и возвращать объект newUser.
//     При вызове функция должна спросить у вызывающего имя и фамилию.
//     Используя данные, введенные пользователем, создать объект newUser со свойствами firstName и lastName.
//     Добавить в объект newUser метод getLogin(), который будет возвращать первую букву имени пользователя, соединенную с фамилией пользователя, все в нижнем регистре (например, Ivan Kravchenko → ikravchenko).
// Создать пользователя с помощью функции createNewUser(). Вызвать у пользователя функцию getLogin(). Вывести в консоль результат выполнения функции.
//
//
//     Необязательное задание продвинутой сложности:
//
//     Сделать так, чтобы свойства firstName и lastName нельзя было изменять напрямую. Создать функции-сеттеры setFirstName() и setLastName(), которые позволят изменить данные свойства.

createNewUser = function () {
    return {
        firstName: prompt("Enter first name") || "NoFirstName",
        lastName: prompt("Enter last name") ||  "NoLastName",
        getLogin: function () {
            return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
        },
    };
};

// Advanced
createNewUser2 = function () {
    const user = {};
    Object.defineProperties(user, {
        "firstName": {
            value: prompt("Enter first name") || "NoFirstName",
            writable: false,
            configurable: true,
            enumerable: true
        },
        "lastName": {
            value: prompt("Enter last name") ||  "NoLastName",
            writable: false,
            configurable: true,
            enumerable: true
        },
        "setFirstName": {
            set(value) {
                Object.defineProperty(this, "firstName", {value})
            },
            enumerable: false,
        },
        "setLastName": {
            set(value) {
                Object.defineProperty(this, "lastName", {value})
            },
            enumerable: false,
        },
        "getLogin": {
            value: function () {
                return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
            },
            enumerable: true,
        }
    });
    return user;
};

let uzver1 = createNewUser();
for(let key in uzver1) {
    console.log(uzver1[key]);
}
let login1 = uzver1.getLogin();
console.log(login1);

let uzver2 = createNewUser2();
for(let key in uzver2) {
    console.log(uzver2[key]);
}
let login2 = uzver2.getLogin();
console.log(login2);

uzver2.firstName = "test-First";
uzver2.lastName = "test-Last";

console.log("First: "+uzver2.firstName+"; Last: "+uzver2.lastName);

uzver2.setFirstName = "Fist";
uzver2.setLastName = "Last";
console.log("First: "+uzver2.firstName+"; Last: "+uzver2.lastName);


uzver2.firstName = "test-First";
uzver2.lastName = "test-Last";

console.log("First: "+uzver2.firstName+"; Last: "+uzver2.lastName);login2 = uzver2.getLogin();
console.log(login2);