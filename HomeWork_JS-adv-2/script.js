const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

const root = document.getElementById('root');
const mainUl = document.createElement('ul');
for (const book of books) {
    const li = document.createElement('li');
    const ul = document.createElement('ul');
    try {
        if (book['author'] === undefined) {
            throw new Error(`noAuthorException in:\n  Book author: ${book['author']}\n  Book name: ${book['name']}\n  Book price: ${book['price']}`);
        }
        if (book['name'] === undefined) {
            throw new Error(`noNameException in:\n  Book author: ${book['author']}\n  Book name: ${book['name']}\n  Book price: ${book['price']}`);
        }
        if (book['price'] === undefined) {
            throw new Error(`noPriceException in:\n  Book author: ${book['author']}\n  Book name: ${book['name']}\n  Book price: ${book['price']}`);
        }

        ul.innerHTML += `<li>Book author: ${book['author']}</li>`;
        ul.innerHTML += `<li>Book name: ${book['name']}</li>`;
        ul.innerHTML += `<li>Book price: ${book['price']}</li>`;
        li.style.marginTop = '1em';
        li.appendChild(ul);
        mainUl.appendChild(li);
    } catch (err) {
        console.log(err)
    }
}
root.appendChild(mainUl);


