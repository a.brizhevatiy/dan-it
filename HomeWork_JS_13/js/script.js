window.onload = () => {
    const blueTheme = {
        'background-image': 'url(img/travel-bg.jpg)',
        'color': '#0099ff',
        'button1-color': '#df4a32',
        'button2-color': '#264b96',
    }
    const greenTheme = {
        'background-image': 'url(img/background.png)',
        'color': '#3cb878',
        'button1-color': '#264b96',
        'button2-color': '#df4a32',
    }
    let currentTheme;
    if (localStorage.getItem('theme')) {
        currentTheme = blueTheme;
    } else {
        currentTheme = greenTheme;
    }
    themeApply();

    const switcher = document.querySelector('.color_switch');
    switcher.addEventListener('click', themeSwitcher);

    function themeSwitcher() {
        if (currentTheme === greenTheme) {
            currentTheme = blueTheme;
            localStorage.setItem('theme', 'blue');
        } else {
            currentTheme = greenTheme;
            localStorage.removeItem('theme');
        }
        themeApply();
    }

    function themeApply() {
        const linksColor = document.querySelectorAll('.link-green, .top-text-link');
        linksColor.forEach(link => link.style.color = `${currentTheme['color']}`);

        const items = document.querySelectorAll('.submit, .green-square');
        items.forEach(item => item.style.backgroundColor = `${currentTheme['color']}`);

        document.querySelector('section').style.backgroundImage = `${currentTheme['background-image']}`;
        document.querySelector('.f-button').style.backgroundColor = `${currentTheme['button1-color']}`;
        document.querySelector('.g-button').style.backgroundColor = `${currentTheme['button2-color']}`;
    }
}