const car2 = {
    'name': 'ford',
    'description': 'England',
};

const car1 = {
    'name': 'toyota',
    'description': 'England',
    'locales': {
        'name': 'lang',
        'description': 'en_UK',
    },
    'contentType': {
        'name': 'content2',
        'description': 'type1',
    },
};

let vehicles2 = [car2]; //массив объектов
const car3 = {
    'name': 'bmw',
    'description': 'Germany',
    'locales': vehicles2,
    'contentType': {
        'name': 'lang',
        'description': 'Japan',
    },
};

const car4 = {
    'name': 'suzuki',
    'description': 'Japan',
    'locales': {
        'name': 'lang',
        'description': 'Germany',
    },
};

let vehicles = [car3, car4]; //массив объектов

let z = filterCollection(vehicles, 'ford lang', false, 'name', 'description', 'contentType.name', 'locales.name', 'contentType.description', 'locales.description')
console.log(z)


function filterCollection(array, words, all, ...keys) {

    if (all) {
        return array.filter(item => words.split(' ').every(word => keys.some(key => findData(item, word, key.split('.')))))
    } else {
        return array.filter(item => words.split(' ').some(word => keys.some(key => findData(item, word, key.split('.')))))
    }
}

function findData(obj, word, key) {
    if (key.length > 1) {
        let elem = key.shift();
        return Array.isArray(obj[elem]) ? arrWalker(obj[elem], word, key) : obj[elem] ? findData(obj[elem], word, key) : false;
    } else {
        return obj[key] ? obj[key].toLowerCase() === word.toLowerCase() : false;
    }
}

function arrWalker(arr, word, key) {
    return arr.some(item => item[key] ? item[key].toLowerCase() === word.toLowerCase() : false);
}