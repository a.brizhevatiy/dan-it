window.onload = function (f) {
    const button = document.querySelector('.circleButton');
    const content = document.querySelector('.content')
    button.addEventListener('click', () => {
        button.remove(); // Удаляю кнопку за ненадобностью

        const input = document.createElement('input'); // Создаю и добавляю поля для ввода радиуса
        const submit = document.createElement('input');
        input.id = 'diameter';
        submit.type = 'submit';
        submit.id = 'diameterSub';
        submit.value = "Нарисовать";
        document.body.append(input);
        document.body.append(submit);

        document.getElementById('diameterSub').onclick = () => {
            let diameter = document.getElementById('diameter').value || '50'; // Получаем размер или берем по умолчанию 50
            for (let item of document.querySelectorAll('input')) { // Удаляю поля за ненадобностью
                item.remove()
            }
            circleGenerate(diameter); // Вызов операция с кругами
        };
    })

    function circleGenerate(diameter) {   // Создание кругов на странице
        for (let i = 0; i < 100; i++) {
            const div = document.createElement('div');
            div.classList.add('circle');
            circleGen(div, diameter); // Вызов генератора
            content.append(div);
        }
        content.addEventListener('click', () => {
            if(event.target.classList.toString() === 'circle'){
                fade(event.target);
            }
        }); // Вызов обработчика
    }

    function circleGen(div, diameter) { // Генератор круга
        div.style = `background-color: #${(Math.random().toString(16) + '000000').substring(2, 8).toUpperCase()};
    width: ${diameter}px; height: ${diameter}px`;
    }

    function fade(div) {  // Плавное удаление круга
        let op = 1;
        const timer = setInterval(() => {
            op -= 0.1;
            div.style.opacity = op;
            if (op <= 0) {
                clearInterval(timer);
                div.remove();
            }
        }, 50);
    }
};