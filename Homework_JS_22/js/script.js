window.onload = () => {
    const table = document.createElement('table');  // Создаем таблицу
    const cells = '<tr>' + '<td></td>'.repeat(29) + '</tr>'; // Создаем строку со столбцами
    table.innerHTML = cells.repeat(29); // Добавляем строку в таблицу (и так 30 раз)
    document.body.appendChild(table); // Добавляем на страницу
    table.addEventListener('click', (event) => { // Ставим отлов кликов на таблицу
        if (event.target.tagName === 'TD') { // Ловим ячейки и...
            event.target.classList.toggle('black') // ... меняем класс
        }
        event.stopPropagation(); // Не пускаем всплытие клика дальше
    })
    window.addEventListener('click', event => { // Вешаем клик на окно браузера
        table.classList.toggle('reverse'); // Добавляем класс в таблицу, чтобы сработал реверс цвета (см. css файл)
    });
}