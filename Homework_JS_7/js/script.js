window.onload = function (f) {
    let newArr = ['hello', ['1', '2', ['1', '2', '3', ['1', '2', '3', 'sea', 'user', 23], 'user', 23], 'sea', 'user', 23], 'Kharkiv', 'Odessa', ['1', '2', '3', 'sea', 'user', 23]];
    // pushToPage(newArr);

    pushToPage(newArr, 100);

    function pushToPage(arr, count) {
        let div = document.createElement("div");
        div.className = "content";
        document.body.append(div);
        div.append(content(arr));
        if (count !== undefined && count > 0) {
            timer(div, count);
        }
    }

    function content(arr) {
        const list = document.createElement('ul');
        arr.map(item => {
            const li = document.createElement('li');
            if (Array.isArray(item)) {
                li.append(content(item));
                return li;
            } else {
                li.innerHTML = item;
                return li;
            }
        }).forEach(teg => {
            list.append(teg);
        });
        return list;
    }

    function timer(div, count) {
        let timer = document.createElement("div");
        timer.className = "timer";
        document.body.append(timer);
        const intervalId = setInterval(() => {
            timer.innerHTML = count;
            count -= 1;
            if (count < -1) {
                timer.classList.add('timerAfter');
                timer.innerHTML = "End";
                div.style.display = "none";
                clearInterval(intervalId);
            }
        }, 1000);
    }
}