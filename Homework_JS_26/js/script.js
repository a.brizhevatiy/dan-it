$(document).ready(function () {
    const tabs = $('.tab');
    const menuTabs = $('.tabs-title');
    menuTabs.on('click', function () {
        menuTabs.removeClass('active');
        $(this).addClass('active');
        tabs.removeClass('active-content');
        const id = `#${$(this).data('item')}`;
        $(id).addClass('active-content');
    });
})