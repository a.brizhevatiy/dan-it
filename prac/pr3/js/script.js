window.onload = () => {
    productsList();

    tegInput({
        tegName: 'h1',
        placeToIns: 'afterend',
        content: 'Some very funny content',
        whichPlaceToIns: 'ul',
        howMany: 10,
    });
}

// напишите функцию, которая будет принимать от пользователя через prompt текст списка товаров
// prompt отображается до тех пор, пока пользователь не нажмет отмена
// все данные введенные пользователем должны быть отображены в виде списка на страинце
// если пользователь ввел пустую сроку - заменить ее на "НЕОПОЗНАННЫЙ ОБЪЕКТ"
function productsList() {
    const ul = document.createElement('ul');
    let item = prompt('Enter the Product');
    while (item !== null) {
        ul.append(fillContent(item, 'li'));
        item = prompt('Enter the Product');
    }
    document.body.appendChild(ul);
}

function fillContent(item, teg) {
    const li = document.createElement(teg);
    if (item === '') {
        li.innerHTML = 'НЕОПОЗНАННЫЙ ОБЪЕКТ';
    } else {
        li.innerHTML = item;
    }
    return li;
}

// напишите функцию, которая принимает аргументом объект с параметрами -
// название тега
// место для вставки (4 возможных опции)
// его содержимое
// в какой элемнент нужно вставить
// количество
function tegInput(obj) {
    const teg = document.querySelector(obj.whichPlaceToIns);
    for (let i = 0; i < obj.howMany; i++) {
        teg.insertAdjacentHTML(obj.placeToIns, `<${obj.tegName}>${obj.content}</${obj.tegName}>`)
    }
}
