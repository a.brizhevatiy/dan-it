import Card from "./Card.js";

export default class Modal {
    // создаем класс для отрисовки модалок
    constructor(dbAgent) {
        this.walker = dbAgent;
        this.main = document.querySelector(".main");
        this.bg = document.createElement("div");
        this.modal = document.createElement("div");
        this.authorZone = document.createElement("div");
        this.titleArea = document.createElement("input");
        this.body = document.createElement("textarea");
        this.authorName = document.createElement("span");
        this.authorMail = document.createElement("span");
        this.buttonArea = document.createElement("div");
        this.okButton = document.createElement("button");
        this.cancelButton = document.createElement("button");
        this.authorName.classList.add("post__author_name");
        this.body.classList.add("modal-body");
        this.authorMail.classList.add("post__email");
        this.titleArea.classList.add("modal-title");
        this.bg.classList.add("modal-bg");
        this.modal.classList.add("modal");
        this.okButton.innerHTML = "Сохранить";
        this.cancelButton.innerHTML = "Отмена";
        this.buttonArea.append(this.okButton, this.cancelButton);
        this.authorZone.append(this.authorName, this.authorMail);
        this.modal.append(
            // формируем модалку
            this.authorZone,
            this.titleArea,
            this.body,
            this.buttonArea
        );
    }

    // отрисовка
    async render(obj = {}) {
        // инициализируем
        this.postId = obj.postId;
        this.userId = obj.userId || 1; // 1 - юзверь по-умоляанию
        this.firstName = obj.firstName || "Leanne";
        this.secondName = obj.secondName || "Graham";
        this.email = obj.email || "Sincere@april.biz";
        this.titleArea.value = obj.title || "";
        this.body.value = obj.body || "";
        this.authorName.innerHTML = obj.firstName // проверка для нужды использования имени по-умолчанию
            ? `${obj.firstName} ${obj.secondName}`
            : "Leanne Graham"; // имя по-умолчанию
        this.authorMail.innerHTML = obj.email || "Sincere@april.biz"; // почта по умолчанию

        this.update = async () => {
            // функция для обновления
            const getEditPost = await this.walker.updateData({
                // отправляем запрос на обновление
                body: {
                    id: this.postId,
                    userId: this.userId,
                    title: this.titleArea.value,
                    body: this.body.value,
                    firstName:this.firstName,
                    secondName:this.secondName,
                    email:this.email,
                },
            });

            const editCard = new Card(getEditPost, this.walker); // создаем пост из полученного ответа
            obj.element.remove(); // удаляем "старый" пост
            document.querySelector(".posts").prepend(await editCard.render()); // добавляем пост с новыми данными на страницу
        };
        // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        //!! зы - рассматривал вариант обновить пост на сервере и из его ответа попытаться обновить ->!!!
        //!! без удаления/добавления карточку, но т.к. тут новые посты не сохраняются на сервере, ->  !!!
        //!! то меня послало лесом и я, соответственно, это не делал и не тестил ((                   !!!
        // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        this.add = async () => {
            console.log(this.walker);
            // функция для добавления
            const getNewPost = await this.walker.addData({
                // отправляем запрос на добавление
                body: {
                    userId: this.userId,
                    title: this.titleArea.value,
                    body: this.body.value,
                    firstName:this.firstName,
                    secondName:this.secondName,
                    email:this.email,
                },
            });
            const newPost = new Card(getNewPost, this.walker); // создаем пост из полученного ответа
            document.querySelector(".posts").prepend(await newPost.render()); // добавляем новый постна страницу
        };

        this.okButton.addEventListener(
            "click",
            async () => {
                // действие по подтверждению
                obj.postId ? this.update() : this.add(); // если есть ID поста - пинаем обновление, иначе пинаем создание
                this.closeFunc(); // закрываем модалку
            },
            { once: true } // избавляемся от создания действия каждый раз при вызове модалки
        );

        this.cancelButton.addEventListener(
            "click",
            () => {
                // действие по отмене
                this.closeFunc(); // закрываем модалку
            },
            { once: true } // избавляемся от создания действия каждый раз при вызове модалки
        );

        document.body.append(this.bg, this.modal); // доавляем модалку на страницу
    }

    closeFunc() {
        //функция закрытия модалки
        this.bg.remove();
        this.modal.remove();
    }
}
