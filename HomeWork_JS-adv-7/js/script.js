import Modal from "./Modal.js";
import Card from "./Card.js";
import Walker from "./BdWalker.js";

const walker = new Walker(); // получаем объект для работы с БД

// создаем и "настраиваем" элементы для страницы
const main = document.querySelector(".main");
const outerAnimationDiv = document.createElement("div");
const innerAnimationDiv = document.createElement("div");
const posts = document.createElement("div");
outerAnimationDiv.append(innerAnimationDiv);
outerAnimationDiv.id = "escapingBallG";
innerAnimationDiv.id = "escapingBall_1";
innerAnimationDiv.classList.add("escapingBallG");
posts.classList.add("posts");
// ---

class Board {
    // Создаем для отображение постов
    constructor(dbAgent) {
        this.walker = dbAgent;
        this.button = document.createElement("button"); // для добаления новых постов
        this.modal = new Modal(this.walker); // Используется для создания новых карточек
    }

    async getPosts() {
        // Получаем данные с сервера
        const resp = walker.getData({});
        return resp;
    }

    async getUsers() {
        // Получаем данные с сервера
        const resp = walker.getUser();
        return resp;
    }

    async render() {
        // Отрисовка
        main.append(outerAnimationDiv); // отображаем анимацию, пока получаются посты
        this.button.classList.add("create-button");
        this.button.innerHTML = "Создать запись";

        this.button.addEventListener("click", async () => {
            // отрисовка Модалки добавления нового поста
            await this.modal.render();
        });

        const postArr = await this.getPosts(); // получаем посты
        const users = await this.getUsers();
        for (const post of postArr) {
            // обходим полученные посты для отрисовки
            const resp = users.find((user) => user.id === post.userId);

            post.firstName = resp.name.split(" ")[0].trim(); // сплитим для более удобной прорисовки имя+фамилия
            post.secondName = resp.name.split(" ")[1].trim(); // сплитим для более удобной прорисовки имя+фамилия
            post.email = resp.email;

            const elem = new Card(post, this.walker);

            posts.prepend(await elem.render());
        }
        outerAnimationDiv.remove(); // убираем анимацию
        main.append(posts); // добавляем посты на страницу
        main.prepend(this.button); // добавляем кнопку для создания постов
    }
}

const test = new Board(walker);

test.render();
