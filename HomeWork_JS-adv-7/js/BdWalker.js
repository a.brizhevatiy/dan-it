export default class BdWalker {
    //Переделал на класс
    constructor() {
        this.API = "https://jsonplaceholder.typicode.com/";
        this.API_USERS = "users";
        this.API_POSTS = "posts";

        this.METHODS = {
            post: "POST",
            get: "GET",
            put: "PUT",
            delete: "DELETE",
        };
    }

    async _connect(data) {
        // Универсальный запрос для альнейшего использования
        let request = {
            method: data.method,
            headers: {
                "Content-type": "application/json; charset=UTF-8",
            },
        };
        let link = this.API + this.API_POSTS;
        if (data.id) {
            // Если запрос с ID то добавляем
            link = link + "/" + data.id;
        }
        if (data.body) {
            // Аналогично если есть карточка, то добавляем в запрос
            request.body = JSON.stringify(data.body); // Если запрос типа GET - данного поля не должно быть - ругнется
        }
        const response = await fetch(link, request);
        const content = await response.json();
        console.log("Тудеть: data: ", data, "link: ", link); // -- > !!!Временно для проверки работоспособности, удалить на релизе!
        console.log("Сюдеть: ", content); // -- > !!!Временно для проверки работоспособности, удалить на релизе!
        return content;
    }

    // ----- Отдельные функции для удаления/изменения/добавления/получения
    async addData(data) {
        return await this._connect({
            method: this.METHODS.post,
            body: data.body,
        });
    }

    async updateData(data) {
        return await this._connect({
            method: this.METHODS.put,
            id: data.body.id,
            body: data.body,
        });
    }

    async removeData(data) {
        return await this._connect({
            method: this.METHODS.delete,
            id: data.id,
        });
    }

    async getData(data) {
        return await this._connect({
            method: this.METHODS.get,
            id: data.id, // если указан ID, то выдаст карточку по данному ID
        });
    }
    async getUser(userId) {
        // отдельно для получения юзверя
        let link = userId
            ? this.API + this.API_USERS + "/" + userId
            : this.API + this.API_USERS;

        let request = {
            method: this.METHODS.get,
        };
        const response = await fetch(link, request);
        const content = await response.json();
        return content;
    }
}
