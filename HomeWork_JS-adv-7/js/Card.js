import Modal from "./Modal.js";

export default class Card {
    // создаем класс для отрисовки карточек, полученных с сервера
    constructor(obj, dbWalker, users) {
        this.users = users;
        this.walker = dbWalker;
        this.obj = obj;
        this.postId = obj.id;
        this.userId = obj.userId;
        this.title = obj.title;
        this.body = obj.body;
        this.firstName = obj.firstName;
        this.secondName = obj.secondName;
        this.email = obj.email;
        this.modal = new Modal(this.walker);
        this.element = document.createElement("div");
    }

    async render() {
        //Создаем
        const authorName = document.createElement("span");
        const authorMail = document.createElement("span");
        const author = document.createElement("div");
        const title = document.createElement("h3");
        const body = document.createElement("p");
        const footer = document.createElement("div");
        //Кнопки удаления и редактирования
        const edit = document.createElement("span");
        const remove = document.createElement("span");
        // ***

        // назначаем классы
        this.element.classList.add("post");
        authorName.classList.add("post__author_name");
        authorMail.classList.add("post__email");
        author.classList.add("post__author");
        title.classList.add("post__title");
        body.classList.add("post__content");
        footer.classList.add("post__footer");
        edit.classList.add("post__edit");
        remove.classList.add("post__remove");

        // Заполняем
        authorName.innerHTML = `${this.firstName} ${this.secondName}`;
        authorMail.innerHTML = this.email;
        title.innerHTML = this.title;
        body.innerHTML = this.body;
        edit.innerHTML = "Редактировать";
        remove.innerHTML = "Удалить";

        // добавляем
        author.append(authorName, authorMail);
        footer.append(edit, remove);
        this.element.append(author);
        this.element.append(title);
        this.element.append(body);
        this.element.append(footer);

        edit.addEventListener("click", async () => {
            // действия по редактированию
            this.modal.render(this);
        });

        remove.addEventListener("click", async () => {
            // действие по удалению
            await this.walker.removeData({ id: this.postId });
            this.element.remove();
        });

        return this.element;
    }
}
