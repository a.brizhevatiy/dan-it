let workersArr = [5,5,5,5]; // Массив из чисел, который представляет скорость работы разных членов команды.
let tasksArr = [25]; // Массив из чисел, который представляет собой беклог (список всех задач, которые необходимо выполнить).
let endDate = new Date('2020-APRIL-13'); // Дата выполнения.

prop(workersArr, tasksArr, endDate);

function prop(speed, backlog, deadline) {
    // Количество рабочих часов в дне:
    const WORK_HOURS = 8;

    // Текущая дата:
    let currDate = new Date();

    // Обнуляем дату для полноценного учета текущего дня:
    currDate.setHours(0, 0, 0, 0);

    // Общая производительность работников за день:
    const peopleDayPoints = speed.reduce(function (sum, current) {
        return sum + current;
    }, 0);

    // Обший объем работы:
    const taskPoints = backlog.reduce(function (sum, current) {
        return sum + current;
    }, 0);

    // Высчитываем разницу дней: дедлайн - текущая:
    const diffDate = ((deadline - currDate) / 86400000);

    // Высчитываем сколько всего дней займет работа:
    const workDays = taskPoints / peopleDayPoints;

    // Перебираем дни срока работы высчитывая рабочие дни, прерываем, если дедлайн наступает до:
    let realDays = 0;
    for (; realDays < workDays;) {
        if (currDate.getDay() !== 0 && currDate.getDay() !== 6) {
            realDays += 1;
        }
        if (currDate.getTime() === endDate.getTime()) {
            break;
        }
        currDate.setDate(currDate.getDate() + 1);
    }

    // Считаем количество часов до окончания работы и дни, оставшиеся до дедлайна:
    const hoursLeft = Math.ceil((workDays - realDays) * WORK_HOURS);
    const howFast = diffDate - realDays;

    // Выводим:
    if (howFast >= 0 && hoursLeft <= 0) {
        console.log(`Все задачи будут успешно выполнены за ${howFast} дней до наступления дедлайна!`);
    } else {
        console.log(`Команде разработчиков придется потратить дополнительно ${hoursLeft} часов после дедлайна, чтобы выполнить все задачи в беклоге.`);
    }
}